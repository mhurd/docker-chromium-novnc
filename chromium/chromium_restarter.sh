#!/bin/bash

set -eu

get_response_code() {
  curl -o /dev/null --silent --head --write-out '%{http_code}' "$1"
}

wait_until_url_available() {
  while ! [[ "$(get_response_code "$1")" =~ ^(200|301)$ ]]; do sleep 1; done
  sleep 0.5
}

# Kicks off Chromium
# Restarts it when it stops
# Writes its debugging url to a text file on each Chromium start
#   This is used by proxy-server.js to provide a stable websocket url for tests
#   so if a tests kills the browser instance, upon retry it still has a working
#   url for the newly restarted Chromium instance
startAndKeepChromiumRunning() {
  local debug_api_base
  debug_api_base="http://127.0.0.1:9222"

  echo "$$" >"chromium_restarter.pid"

  while true; do
    echo "(re)starting chromium..."

    /usr/bin/chromium \
      --debug-devtools \
      --disable-background-networking \
      --disable-background-timer-throttling \
      --disable-backgrounding-occluded-windows \
      --disable-breakpad \
      --disable-client-side-phishing-detection \
      --disable-component-extensions-with-background-pages \
      --disable-default-apps \
      --disable-dev-shm-usage \
      --disable-extensions \
      --disable-features=site-per-process,TranslateUI,BlinkGenPropertyTrees \
      --disable-gpu \
      --disable-gpu-sandbox \
      --disable-hang-monitor \
      --disable-ipc-flooding-protection \
      --disable-popup-blocking \
      --disable-prompt-on-repost \
      --disable-renderer-backgrounding \
      --disable-setuid-sandbox \
      --disable-sync \
      --enable-automation \
      --enable-features=NetworkService,NetworkServiceInProcess \
      --force-fieldtrials=*BackgroundTracing/default/ \
      --metrics-recording-only \
      --mute-audio \
      --no-default-browser-check \
      --no-first-run \
      --no-sandbox \
      --password-store=basic \
      --remote-debugging-port=9222 \
      --single-process \
      --start-maximized \
      --use-mock-keychain \
      --user-data-dir=/home/chromium/profile \
      --window-position=0,0 \
      --window-size=1280,1024 \
      --disable-component-update \
      --enable-logging=stderr \
      --v=1 \
      http://localhost:9222 >/dev/null 2>&1 &

    # Get the Chromium process ID
    local chromium_pid
    chromium_pid=$!

    echo "$chromium_pid" >"chromium.pid"

    # Wait for Chromium to start and the remote debugging API to become available
    wait_until_url_available "${debug_api_base}/json/version"

    # Fetch the WebSocket URL and update the ws-debugger-url.txt file
    curl --silent "${debug_api_base}/json/version" |
      awk -F\" '/webSocketDebuggerUrl/ {gsub("127.0.0.1", "0.0.0.0", $4); printf "%s", $4}' >/app/ws-debugger-url.txt

    # Wait for the Chromium process to exit
    wait $chromium_pid

    rm -f "chromium.pid"

    rm -f /app/ws-debugger-url.txt
  done
}

killChromiumProcesses() {
  echo "killing any running chromium processes including the 'chromium_restarter.sh' script"
  local pid

  if [ -f "./chromium_restarter.pid" ]; then
    pid=$(cat "./chromium_restarter.pid")
    echo "killing chromium_restarter.sh process $pid"
    kill "$pid"
    rm -f "./chromium_restarter.pid"
  fi

  if [ -f "./chromium.pid" ]; then
    pid=$(cat "./chromium.pid")
    echo "killing chromium process $pid"
    kill "$pid"
    rm -f "./chromium.pid"
  fi
}

main() {
  startAndKeepChromiumRunning
}

# If this script is being run (not sourced), then run the main function
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  main
fi
