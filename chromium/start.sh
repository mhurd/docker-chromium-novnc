#!/bin/bash

set -eu

# Give the novnc container a bit to set up its virtual frame buffer display bits
sleep 1

# If CHROMIUM_VERSION environment var is set when the container is built, Chromium
# will be present in the image when it is brought up. If we see Chromium is present
# the following script starts it in automation mode and restarts it if it gets closed
if [ -f "/usr/bin/chromium" ]; then
  ./chromium_restarter.sh &
fi

node proxy-server.js
