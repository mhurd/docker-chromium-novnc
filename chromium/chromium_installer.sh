#!/bin/bash

set -eux

source ./chromium_restarter.sh

# .deb Chromium installer files indicate architecture by abbreviations which differ slightly from those returned by "uname -m"
# see pages like this for examples: http://snapshot.debian.org/package/chromium/112.0.5615.138-1~deb11u1/#chromium_112.0.5615.138-1:7e:deb11u1
mapUnameArchToDebFileArch() {
  case "$1" in
  aarch64)
    echo "arm64"
    ;;
  x86_64)
    echo "amd64"
    ;;
  i686 | i386)
    echo "i386"
    ;;
  *)
    echo "unsupported architecture"
    ;;
  esac
}

removeChromium() {
  echo "removing chromium if installed"
  apt-get remove -y chromium chromium-common
  rm -f /usr/bin/chromium /usr/bin/chromium-browser /usr/bin/chrome
}

fetchDebUrls() {
  local debArch
  debArch=$(mapUnameArchToDebFileArch "$(uname -m)")
  node -e "
  const debFinder = require('chromium-version-deb-finder');
  (async () => {
    const version = '$1'.split('.');
    const debianVersion = 11;
    const debUrls = await debFinder.getDebUrlsForVersionAndArch(version[0], version[1], debianVersion, '$debArch');
    console.log(debUrls.join('\n'));
  })();"
}

downloadDebFiles() {
  local debFiles
  debFiles=()
  while read -r url; do
    local fileName
    fileName=$(basename "$url")
    if [ -f "/app/chromium_debs/$fileName" ]; then
      echo "File /app/chromium_debs/$fileName already exists. Skipping download." >&2
      debFiles+=("/app/chromium_debs/$fileName")
    else
      echo "Downloading: $url" >&2
      mkdir -m 755 -p /app/chromium_debs
      wget -c -t 10 -w 10 -T 120 -P /app/chromium_debs "$url" && debFiles+=("/app/chromium_debs/$fileName")
      if [ $? -eq 0 ]; then
        echo "Downloaded: $url" >&2
      else
        echo "Failed to download: $url" >&2
      fi
    fi
  done
  echo "${debFiles[@]}"
}

installChromium() {
  chromium --version || echo "chromium is not installed"
  echo "installing chromium version $1"

  apt-get install -y git sudo software-properties-common

  local debUrls
  debUrls=$(fetchDebUrls "$1")

  local debFiles
  debFiles=($(echo "$debUrls" | downloadDebFiles))

  local debCount
  debCount=${#debFiles[@]}

  if [ "$debCount" -ne 2 ]; then
    echo "Error: Expected two .deb files, but found $debCount. Exiting."
    exit 1
  fi
  apt install -y "${debFiles[@]}" &&
    test -f /usr/bin/chromium && ln -s /usr/bin/chromium /usr/bin/chromium-browser && ln -s /usr/bin/chromium /usr/bin/chrome
  echo "installed chromium version $1"
}

main() {
  apt-get update
  killChromiumProcesses "$1"
  removeChromium "$1"
  installChromium "$1"
}

# If this script is being run (not sourced), then run the main function
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  main "$1"
fi
