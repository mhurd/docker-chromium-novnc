const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const http = require('http');
const { spawn } = require('child_process');
const { createProxyMiddleware } = require('http-proxy-middleware');

process.on('uncaughtException', (err) => {
  console.error('Uncaught exception:', err);
});

const app = express();
const PORT = 3111;

const readEndpointIdFromFile = () => {
  try {
    const wsDebuggerUrlFile = path.join(__dirname, 'ws-debugger-url.txt');
    const endpoint = fs.readFileSync(wsDebuggerUrlFile, 'utf8');
    return endpoint.trim();
  } catch (error) {
    console.error('Error reading endpoint from file:', error);
    return null;
  }
};

// web sockets proxy
const wsProxy = createProxyMiddleware('/ws', {
  target: 'ws://0.0.0.0:9222',
  changeOrigin: true,
  ws: true,
  router: (req) => {
    const endpoint = readEndpointIdFromFile();
    if (endpoint) {
      console.log('Routing request to:', endpoint);
      return endpoint;
    } else {
      console.error('Could not read endpoint from file');
      return null;
    }
  },
  onProxyReqWs: (proxyReq, req, res, options) => {
    const endpoint = readEndpointIdFromFile();
    if (endpoint) {
      console.log('New WebSocket connection:', endpoint);
      options.target = endpoint;
    } else {
      console.error('Could not read endpoint from file');
    }
  },
  onError: (err, req, res) => {
    console.error('Proxy error:', err);
  },
});

app.use(wsProxy);

// method for invoking shell script
const runShellScript = (res, scriptName, scriptParams = []) => {
  return new Promise((resolve, reject) => {
    const scriptPath = path.join(__dirname, scriptName);
    const child = spawn('bash', [scriptPath, ...scriptParams]);

    child.stdout.on('data', (data) => {
      console.log(data);
      res.write(data);
    });

    child.stderr.on('data', (data) => {
      console.error(data);
      res.write(data);
    });

    child.on('close', (code) => {
      if (code !== 0) {
        console.error(`exited with code ${code}`);
        reject(new Error(`exited with code ${code}`));
      } else {
        resolve();
      }
    });
  });
};

// route for starting and restarting chromium if it is shut down by a selenium test
router.get('/chromiumRestarter', async (req, res) => {
  console.log('running chromium_restarter.sh');
  try {
    await runShellScript(res, 'chromium_restarter.sh');
    res.end('finished chromium_restarter.sh');
  } catch (err) {
    res.status(500).send(`error running chromium_restarter.sh: ${err.message}`);
  }
});

// route for triggering chromium installation
router.get('/installChromium/:version', async (req, res) => {
  const version = req.params.version;
  console.log(`received request to install Chromium version ${version}`);
  try {
    await runShellScript(res, 'chromium_installer.sh', [version]);
    res.end(`finished installing Chromium version ${version}\n`);
  } catch (err) {
    res.status(500).send(`error installing Chromium version ${version}: ${err.message}`);
  }
});

app.use(router);

// define the proxy for /json/version
// provides easy way to confirm chromium is ready (by checking it for 200 response)
// note: its return json should not be used to determine chromium's automation websocket url
// (it's MUCH easier to use the websocket proxy above for that)
const jsonVersionProxy = createProxyMiddleware('/json/version', {
  target: 'http://0.0.0.0:9222',
  changeOrigin: true,
  onError: (err, req, res) => {
    console.error('Proxy error:', err);
    res.status(500).send('Error proxying /json/version');
  }
});

app.use(jsonVersionProxy);

// error handling
app.use((err, req, res, next) => {
  if (err.code === 'ECONNREFUSED') {
    console.error('Connection refused:', err.message);
    res.status(404).send('WebSocket endpoint not available');
  } else {
    next(err);
  }
});

const server = app.listen(PORT, () => { 
  console.log(`Server is running on port ${PORT}`); 
});

// support initial websocket connection (vs http)
server.on('upgrade', wsProxy.upgrade);