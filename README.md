![Diagram](diagram.png "Diagram")

Run `./fresh_install` to spin up

Provides endpoint for installing a specific version of Chromium

`curl http://docker-chromium-novnc-chromium-1:3111/installChromium/99.0.4844.84` 

Provides endpoint for starting Chromium in automation mode and keeping it running if a test closes it

`curl http://docker-chromium-novnc-chromium-1:3111/chromiumRestarter`

Alternatively, if `./fresh_install` is run with an environment variable specifying the desired Chromium version, it will attempt to install that version in the "chromium" Docker image it creates

If Chromium is present in the image, the "chromiumRestarter" is automatically invoked when the container is brough up

`CHROMIUM_VERSION=99.0.4844.0 ./fresh_install`

Provides stable websocket endpoint for driving Chromium automation, commonly via Puppeteer

By "stable" it is meant that a different url isn't needed every time Chromium gets restarted - this means, for example, if WebDriver is in use, its [`maxInstances`](https://webdriver.io/docs/configuration/#maxinstances) property should be set to 1. As the current main usecase is watching automation tests execute while debugging them, the single instance restriction makes sense for now

`ws://docker-chromium-novnc-chromium-1:3111/ws`

Provides noVNC endpoint so Chromium automation can be viewed externally

`http://localhost:8088/vnc_lite.html?autoconnect=true&resize=scale`

The raw `/json/version` route served by automation mode Chromium may also be accessed. You should prefer the "stable" websocket endpoint above for driving Chromium automation. `/json/version` is proxied simply so you can monitor it for 200 http response to know when Chromium is automation ready 

`http://localhost:3111/json/version`

Note:

The `http://docker-chromium-novnc-chromium-1` urls above are how you'd address endpoints from within other Docker containers on the same Docker network

You could replace it with `http://localhost` if trying to hit the endpoints from the Docker host OS